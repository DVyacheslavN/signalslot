#ifndef FACTORYHANDLER_H
#define FACTORYHANDLER_H

namespace Adapter
{
class WatcherBase;
}

namespace Factory
{

typedef enum class WatcherTypes : int
{
    WatcherPeriodic = 0,
    WatcherAsync,
} WatcherTypes;

class FactoryHandler
{
public:
    static FactoryHandler *instance();

    Adapter::WatcherBase *createWatcher(WatcherTypes watcherType);
private:
    FactoryHandler();

};
}
#endif // FACTORYHANDLER_H

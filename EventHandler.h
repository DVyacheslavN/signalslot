#ifndef EVENT_HANDLER
#define EVENT_HANDLER

#include <functional>
#include <iostream>
#include <type_traits>
#include <utility>

#include "FactoryHandler.h"
#include "HandlerAdapter.h"

#include <debug.h>

#ifdef EVENTS_DEBUG
#define debug() mDebug()
#else
#define debug() mCork()
#endif

class EventHandler;
template<class T>
class Signal;

using namespace Factory;

template <class T, class O>
class Function1Atrb : public Adapter::Function
{
public:
    Function1Atrb(T *object, void (T::*f)(O data))
    {
        m_function = std::bind(f, object, std::placeholders::_1);
    }
    ~Function1Atrb() override {}

    template<typename C, typename M>
    std::enable_if_t<!std::is_pointer<C>::value, O> rmPtr(M t) {

        debug() << "data:" << static_cast<O *>(t);
        return *(static_cast<O *>(t));
    }

    template<typename C, typename M>
    std::enable_if_t<std::is_pointer<C>::value && !std::is_same<C, M>::value , O> rmPtr(M& t) {

        debug() << "data:" << *static_cast<O>(t);
        return static_cast<O>(t);
    }

    template<typename C, typename M>
    std::enable_if_t<std::is_same<C, M>::value, M> rmPtr(M t) {
        return (t);
    }

    virtual void call(void *data) override
    {
        debug() << "call data ptr:" << data;
        O tmp = rmPtr<O>(data);
        if (m_function != nullptr)
        {
            m_function(tmp);
        }
    }

private:
    std::function<void (O)> m_function;
};

template<class T>
class Signal
{
    friend EventHandler;

public:
    Signal() : m_watcher(nullptr){}
    ~Signal()
    {
        if (m_watcher != nullptr)
        {
            debug() << "delete watcher";
            delete m_watcher;
        }
    }

    template<typename M>
    std::enable_if_t<std::is_pointer<M>::value, T&> addPtr(M& t) {
        debug() << "data:" << t;
        return t;
    }

    template<typename M>
    std::enable_if_t<!std::is_pointer<M>::value, std::add_pointer_t<T>> addPtr(M& t) {

        M* tmp = new M(t);
        debug() << "data:" << *tmp;
        return (tmp);
    }

    void call(T data)
    {
        if (m_watcher == nullptr)
        {
            debug() << "watcher is null";
            return;
        }

        m_watcher->setDataPtr(addPtr<T>(data));
        m_watcher->send();
    }
private:
    Adapter::WatcherBase *m_watcher;
};

class EventHandler : public Adapter::EventHandlerBase
{

public:
    EventHandler() {}
    ~EventHandler() override {}

    template<class O, class K>
    void connect(Signal<O> *sig, K *object, void (K::*f)(O))
    {
        Adapter::WatcherBase *watcher = FactoryHandler::instance()->createWatcher(WatcherTypes::WatcherAsync);
        watcher->setFunction(new Function1Atrb<K, O>(object, f));
        watcher->setHandler(this);
        watcher->start();
        sig->m_watcher = watcher;
    }
};


namespace Handler {

class WatcherAsync : public Adapter::AsyncWatcherBase
{
public:
    WatcherAsync() : Adapter::AsyncWatcherBase()
    {}

    ~WatcherAsync() override
    {}

    template<class O, class K>
    void setCallback(K *object, void (K::*f)(O))
    {
        setFunction(new Function1Atrb<K, O>(object, f));
        start();
    }
};

class WatcherPeriodic : public Adapter::PeriodicWatcherBase
{
public:
    WatcherPeriodic()
    {}

    ~WatcherPeriodic() override
    { stop(); }

    template<class O, class K>
    void setCallback(K *object, void (K::*f)(O))
    {
        setFunction(new Function1Atrb<K, O>(object, f));
    }
};
}

#endif

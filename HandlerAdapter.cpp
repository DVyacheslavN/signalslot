#include "HandlerAdapter.h"
#include <ev++.h>
#include <debug.h>

using namespace Adapter;

#ifdef EVENTS_DEBUG
    #define debug() mDebug()
#else
#define debug() mCork()
#endif

EventHandlerBase::EventHandlerBase()
    : m_loop(ev_loop_new())
{}

EventHandlerBase::~EventHandlerBase()
{

}

void EventHandlerBase::run()
{
    debug() << "loop ptr:" << m_loop;
    while (1) {
        ev_run(m_loop);
        sleep(1);
    }

    debug() << "stop" << m_loop;
}

WatcherBase::WatcherBase()
{

}

WatcherBase::~WatcherBase()
{

}

void WatcherBase::call(void *data)
{
    if (m_function == nullptr)
    {
         debug() << "callBack is nullptr";
         return;
    }
    m_function->call(data);
}

void WatcherBase::setFunction(Function *function)
{
    m_function = function;
}

void WatcherBase::setDataPtr(void *data)
{
    m_data = data;
}

AsyncWatcherBase::AsyncWatcherBase() : m_async(new ev::async)
{
    m_async->set(this);
}

AsyncWatcherBase::~AsyncWatcherBase()
{
    delete m_async;
}

void AsyncWatcherBase::start()
{
    m_async->start();
}

void AsyncWatcherBase::send()
{
    m_async->send();
}

void AsyncWatcherBase::setHandler(EventHandlerBase *handler)
{
    m_async->set(handler->m_loop);
}

void AsyncWatcherBase::operator()(ev::async &w, int revents)
{
    call(m_data);
}

//---------Periodic watcher base---------//

void PeriodicWatcherBase::start()
{
    if (m_periodic == nullptr || m_periodic->loop == nullptr)
    {
        debug () << "null";
    }
    m_periodic->start(m_timeStamp, m_interval);
}

void PeriodicWatcherBase::stop()
{
    m_periodic->stop();
}

void PeriodicWatcherBase::send()
{
    debug() << "send event";
    ev_invoke(m_periodic->loop, m_periodic, 0);
}

void PeriodicWatcherBase::setHandler(EventHandlerBase *handler)
{
    m_periodic->set(handler->m_loop);
}

void PeriodicWatcherBase::operator()(ev::periodic &w, int revents)
{
    w.interval += w.offset;
    call(m_data);
}

void PeriodicWatcherBase::setTimeStamp(ev_tstamp timeStamp)
{
    m_timeStamp = timeStamp;
    debug() << "set time stamp:" << timeStamp;
}

void PeriodicWatcherBase::setTimeInteval(ev_tstamp interval)
{
    m_interval = interval;
    debug() << "set interval:" << m_interval;
}

PeriodicWatcherBase::PeriodicWatcherBase()
    : m_periodic(new ev::periodic), m_interval(1), m_timeStamp(1)
{
    m_periodic->set(this);
}

PeriodicWatcherBase::~PeriodicWatcherBase()
{
    delete m_periodic;
}

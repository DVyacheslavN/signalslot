#include "EventHandler.h"
#include <ev++.h>

#ifdef EVENTS_DEBUG
    #define debug() mDebug()
#else
#define debug() mCork()
#endif

#ifdef AA
using namespace Handler;

//-----------------WatcherAsync---------------//
WatcherAsync::WatcherAsync() : m_data(nullptr), m_async(new ev::async)
{
    m_async->set(this);
}

WatcherAsync::~WatcherAsync()
{
    delete m_async;
}

void WatcherAsync::setDataPtr(void *ptr)
{
    debug() << "data ptr:" << ptr;
    m_data = ptr;
}

void WatcherAsync::operator()(ev::async &w, int revents)
{
    if (function == nullptr)
    {
         debug() << "callBack is nullptr";
         return;
    }
    function->call(m_data);
}

void WatcherAsync::setHandler(EventHandler *handler)
{
    debug() << "handler ptr:" << handler;
    m_async->set(handler->m_loop);
}

void WatcherAsync::start()
{
    m_async->start();
}

void WatcherAsync::send()
{
    debug() << "send event";
    m_async->send();
}
//-------------***WatcherAsync***------------//



//--------------WatcherPeriodic--------------//
WatcherPeriodic::WatcherPeriodic() :  m_data(nullptr), m_periodic(new ev::periodic)
{
    m_periodic->set(this);
    setTimeStamp(1);
    setTimeInteval(1);
}

WatcherPeriodic::~WatcherPeriodic()
{
    delete m_periodic;
}

void WatcherPeriodic::setDataPtr(void *ptr)
{
    m_data = ptr;
}

void WatcherPeriodic::setHandler(EventHandler *handler)
{
    m_periodic->set(handler->m_loop);
}

void WatcherPeriodic::operator()(ev::periodic &w, int revents)
{
    w.interval += w.offset;
    if (function == nullptr)
    {
        debug() << "callBack is nullptr";
        return;
    }
    function->call(m_data);
}

void WatcherPeriodic::setTimeStamp(ev_tstamp timeStamp)
{
    debug() << "set time stamp:" << timeStamp;
    m_timeStamp = timeStamp;
}

void WatcherPeriodic::setTimeInteval(ev_tstamp interval)
{
    m_interval = interval;
    debug() << "set interval:" << m_interval;
}

void WatcherPeriodic::start()
{
    if (m_periodic == nullptr || m_periodic->loop == nullptr)
    {
        debug () << "null";
    }
    m_periodic->start(m_timeStamp, m_interval);
}

void WatcherPeriodic::send()
{
    debug() << "send event";
    ev_invoke(m_periodic->loop, m_periodic, 0);
}
#endif
//-----------***WatcherPeriodic***-----------//


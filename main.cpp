#define SIMPLE_TEST
#define TEST
#ifdef GENERAL_TEST

#elif defined(TEST)

#include "EventHandler.h"
#include <iostream>
#include <string>

#include<thread>

class Test
{
public:
    Test() {}
    void first(int *data)
    {
        std::cerr << "TestFirst:" << *data;//*(static_cast<std::string *>(data));
    }

    void second(int data)
    {
        std::cerr << "TestSecond:" << data ;//<< *data;//*(static_cast<std::string *>(data));
    }

    void third(void *data)
    {
        std::cerr << "TestThird" << *(static_cast<std::string *>(data));
    }

    void periodik(void *data)
    {
        std::cerr << "testPerodik" << std::endl; //<< *(static_cast<std::string *>(data));
    }
};

//____________ Test event handler ____________//

int main()
{

    std::cerr << "START" << std::endl;
    EventHandler *handler = new EventHandler; // create handler
    auto asingSignFirst = new Signal<int *>(); // create signal Object
    auto asyncSignSecond = new Signal<int>(); // create signal Object
    auto asyncSignThird = new Signal<void *>();

    //     create test object
        Test *test = new Test;

    // his methods will call when signal is sent
    //handler->connect<int,Test, &Test::first >(asingSignFirst, test, &Test::first);
//    handler->connect(asingSignFirst, test, &Test::first);
//    handler->connect(asyncSignSecond, test, &Test::second);
//    handler->connect(asyncSignThird, test, &Test::third);

    Handler::WatcherPeriodic periodik;
    periodik.setTimeStamp(0.0);
    periodik.setTimeInteval(0.5);


    std::string str("DDDDDDD");
    int *xd = new int(5);
    asingSignFirst->call(xd);
    asyncSignSecond->call(10);
    asyncSignThird->call(&str);

    std::thread th(std::bind(&EventHandler::run, handler));
    th.detach();


    periodik.setHandler(handler);
    periodik.setCallback(test, &Test::periodik);
    periodik.start();
    std::string str1;
    std::cin >> str1;
}

//\\\\\\\\\\\\\\ Test event handler \\\\\\\\\\\\\\//
#elif defined (SIMPLE_TEST)

//________________simple test_______________//


#include <ev++.h>
#include <iostream>

struct ev_loop *thLoop;
static bool initWatcher = false;
ev_async *async_watcher;


void async_cb (EV_P_ ev_async *w, int revents)
{
    std::cerr << "ASYNC";
}

void cb_HandleEvent(struct ev_loop *loop, ev_periodic *w, int revents)
{
    w->interval = w->interval + w->offset;
    for (int i = 0; i < 100000000; i++)
    {

    }

    if (!initWatcher)
    {
        initWatcher = true;

        async_watcher = new ev_async;
        ev_async_init(async_watcher, async_cb);
        ev_async_start(loop, async_watcher);
        ev_async_send(loop, async_watcher);

    }

    std::cerr << "PERIODIC" << "w->offset:" << w->offset << "  w->interval:" << w->interval << std::endl;
}



int main()
{
    struct ev_loop *loop = ev_loop_new(0);
    thLoop = ev_loop_new(0);
    ev_periodic periodic_watcher;

     ev_periodic_init(&periodic_watcher, cb_HandleEvent,0.0, 0.5, 0);
     ev_periodic_start(loop, &periodic_watcher);

     ev_run(loop, 0);
}

//|||||||||||||||simple test|||||||||||||||//

#endif

    //        Handler::WatcherPeriodic *period = new Handler::WatcherPeriodic;
    //        period->setTimeStamp(1000.);
    //        period->setTimeInteval(1.);
    //        std::string *strdd = new std::string("callbackString");
    //        period->setCallback(test, &Test::periodik);

    //        period->setHandler(handler);
    //        period->start();
    //        handler->run();

//-------------------------periodik--------simply
//    struct ev_loop *loop = ev_default_loop();
//    WatcherPeriod *period = new WatcherPeriod;
//    std::string *strdd = new std::string( "callbackString");
//        period->m_data = strdd;
//    period->m_callback = std::bind(&Test::dddd, test, std::placeholders::_1);
//    std::string *dsfsdfsd = new std::string( "COOOODDDDDDSSS");
//        period->m_data = dsfsdfsd;


//    period->set(loop);
//    period->start();
//    ev_run(loop);


//------------------------------//
/*
#include <iostream>

using namespace std;

static void async_cb (EV_P_ ev_async *w, int revents);

void cb_HandleEvent(struct ev_loop *loop, ev_periodic *w, int revents);

static void thrun ();

struct ev_loop *loop2;

ev_periodic periodic_watcher;
ev_async async_watcher;

int main()
{
    cout << "Hello World!" << endl;

   struct ev_loop *loop = ev_default_loop();
    loop2 = ev_loop_new(0);

    ev_periodic_init(&periodic_watcher, cb_HandleEvent, 0., 5., 0);
    ev_periodic_start(loop, &periodic_watcher);

//    ev_timer_init (&timeout_watcher, timeout_cb, 5.5, 0.);
//    ev_timer_start (loop, &timeout_watcher);


//    ev_async_init(&async_watcher, async_cb);
//    ev_async_start(loop2, &async_watcher);


//    ConnectivityEngine *connect = new ConnectivityEngine;
//    TcpPackage package;
//    mDebug() << "size:" << sizeof(package);
//    connect several clients
//    ip::tcp::endpoint ep( ip::address::from_string("127.0.0.1"), 2323);
//    std::cerr << "Create Engine\n";
//    mMessage() << "startServer";
//    Server *ser= new Server("127.0.0.1", 2323);

    /*---------------Tcp Parser Test----------------*/
/*
        std::vector<int> list {10, 11, 12, 13, 14, 15};
        char * oooo =  data.parseTypeToData(list);

         auto aaa = data.parseTypeToPackage<std::vector<int>>(list, 1111, 33333);

        mDebug() << "Package:" << "type:" << aaa->head.typeId << "data:" << data.parseDataToType<std::vector<int>>(aaa->buf)->at(2);
        mDebug() << "parser:" << (data.parseDataToType<std::vector<int>>(oooo))->at(2);
    /*-------------------------------*/



//std::thread th (&thrun);
//th.detach();
/*
 ev_run(loop, 0);

    return 0;
}
/*
static void thrun ()
{
    std::cerr << "thrun thread id:" << this_thread::get_id() << cr;
     ev_run(loop2, 0);

}

static void
async_cb (EV_P_ ev_async *w, int revents)
{
  puts ("timeout");
    std::cerr << "async_cb thread id:" << this_thread::get_id() << cr;
//  ev_timer_init (&timeout_watcher, timeout_cb, 5.5, 0.);
//  ev_timer_start (loop, &timeout_watcher);

//  std::cerr << "timeout_cb thread id:" << this_thread::get_id() << cr;

//  if (ev_async_pending(&async_watcher)==false) { //the event has not yet been processed (or even noted) by the event loop? (i.e. Is it serviced? If yes then proceed to)
//      ev_async_send(loop, &async_watcher); //Sends/signals/activates the given ev_async watcher, that is, feeds an EV_ASYNC event on the watcher into the event loop.
//  }

  // this causes the innermost ev_run to stop iterating
 // ev_break (EV_A_ EVBREAK_ONE);
 // ev_timer_init (w, timeout_cb, 5.5, 0.);
}

void cb_HandleEvent(struct ev_loop *loop, ev_periodic *w, int revents)
{
    std::cerr << "cb_HandleEvent thread id:" << this_thread::get_id() << cr;

//    if (ev_async_pending(&async_watcher)==false) { //the event has not yet been processed (or even noted) by the event loop? (i.e. Is it serviced? If yes then proceed to)
//        ev_async_send(loop2, &async_watcher); //Sends/signals/activates the given ev_async watcher, that is, feeds an EV_ASYNC event on the watcher into the event loop.
//    }
    ev_invoke(loop2, &async_watcher, 0);

}
*/

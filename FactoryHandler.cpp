#include "FactoryHandler.h"
#include "EventHandler.h"
#include "HandlerAdapter.h"

using namespace Handler;
using namespace Factory;
using namespace Adapter;


FactoryHandler::FactoryHandler()
{

}

FactoryHandler *FactoryHandler::instance()
{
    static FactoryHandler factory;
    return &factory;
}

WatcherBase *FactoryHandler::createWatcher(WatcherTypes watcherType)
{
    switch (watcherType)
    {
    case WatcherTypes::WatcherAsync:
        return static_cast<WatcherBase *>(new WatcherAsync);
    case WatcherTypes::WatcherPeriodic:
        return static_cast<WatcherBase *>(new WatcherPeriodic);
    default:
        return nullptr;
    }
}

#ifndef HANDLERADAPTER_H
#define HANDLERADAPTER_H

typedef double ev_tstamp;
typedef struct ev_loop;

namespace ev {
class async;
class periodic;
}

namespace Adapter {

class AsyncWatcherBase;
class PeriodicWatcherBase;

class Function
{
public:
    Function () {}
    virtual ~Function(){}
    virtual void call(void *data_ptr) = 0;
};

class EventHandlerBase
{
    friend AsyncWatcherBase;
    friend PeriodicWatcherBase;
public:
    void run();

protected:
    EventHandlerBase();
    virtual ~EventHandlerBase();

    void *m_data; //?????????????????//
private:
    ev_loop * const m_loop;
};

class WatcherBase
{
public:
    virtual void start() = 0;
    virtual void send() = 0;
    virtual void setHandler(EventHandlerBase *handler) = 0;
    virtual void setFunction(Function *function);
    virtual void setDataPtr(void *data);
    virtual ~WatcherBase();

protected:
    WatcherBase();
    void call(void *data);

    void *m_data;
private:
    Function *m_function;
};


class AsyncWatcherBase : public WatcherBase
{
public:
    virtual void start() override;
    virtual void send() override;
    virtual void setHandler(EventHandlerBase *handler) override;
    void operator()(ev::async &w, int revents);

protected:
    AsyncWatcherBase();
    ~AsyncWatcherBase() override;

private:
    ev::async * const m_async;

};

class PeriodicWatcherBase : public WatcherBase
{
public:
    virtual void start() override;
    void stop();
    virtual void send() override;
    virtual void setHandler(EventHandlerBase *handler) override;
    void operator()(ev::periodic &w, int revents);

    void setTimeStamp(ev_tstamp timeStamp);
    void setTimeInteval(ev_tstamp interval);


protected:
    PeriodicWatcherBase();
    ~PeriodicWatcherBase() override;

private:
    ev::periodic * const m_periodic;
    ev_tstamp m_interval;
    ev_tstamp m_timeStamp;
};

}
#endif // HANDLERADAPTER_H
